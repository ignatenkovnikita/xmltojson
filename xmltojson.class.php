<?php

class XmlToJson { // Создаем новый класс

	public function ParseUrl ($url) { // Объявляем функцию

		$fileContents= file_get_contents($url); // Получаем контент по ссылки

		$fileContents = str_replace(array("\n", "\r", "\t"), '', $fileContents); // Убираем ненужные символы

		$fileContents = trim(str_replace('"', "'", $fileContents)); // Заменяем кавычки на одинарные и убираем пробелы

		$simpleXml = simplexml_load_string($fileContents); // Интерпретирует строку с XML в объект

		$json = json_encode($simpleXml); // Возвращает JSON-представление данных

		return $json;

	}

	public function ParseText ($text) {


		$text = str_replace(array("\n", "\r", "\t"), '', $text);

		$text = trim(str_replace('"', "'", $text));

		$simpleXml = simplexml_load_string($text);

		$json = json_encode($simpleXml);

		return $json;

	}

}

?>